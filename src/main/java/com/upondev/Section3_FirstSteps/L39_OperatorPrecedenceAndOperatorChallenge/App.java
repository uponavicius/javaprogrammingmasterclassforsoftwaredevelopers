package com.upondev.Section3_FirstSteps.L39_OperatorPrecedenceAndOperatorChallenge;

public class App {
    public static void main(String[] args) {
        //Summary of Operators https://docs.oracle.com/javase/tutorial/java/nutsandbolts/opsummary.html
        //Java Operator Precedence Table http://www.cs.bilkent.edu.tr/~guvenir/courses/CS101/op_precedence.html

        double myFirstValue = 20.00d;
        double mySecondValue = 80.00d;
        double myValuesTotal = (myFirstValue + mySecondValue) * 100.00d;
        System.out.println("MyValuesTotal = " + myValuesTotal);
        double theRemainder = myValuesTotal % 40.00d;
        System.out.println("theRemainder = " + theRemainder);
        boolean isNoRemainder = (theRemainder == 0) ? true : false;
        System.out.println("isNoRemainder = " + isNoRemainder);
        if (!isNoRemainder) {
            System.out.println("Got some remainder");

        }
    }
}