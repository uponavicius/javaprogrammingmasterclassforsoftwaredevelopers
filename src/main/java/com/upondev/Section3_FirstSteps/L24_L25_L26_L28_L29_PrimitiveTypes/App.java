package com.upondev.Section3_FirstSteps.L24_L25_L26_L28_L29_PrimitiveTypes;

public class App {
    public static void main(String[] args) {
        //L24 Primitive Types
        int myValue = 10000;

        int myMinIntValue = Integer.MIN_VALUE;
        int myMaxIntValue = Integer.MAX_VALUE;
        System.out.println("Integer Minimum Value = " + myMinIntValue);
        System.out.println("Integer Maximum Value = " + myMaxIntValue);
        System.out.println("Busted MAX Value = " + (myMaxIntValue + 1)); //overflow
        System.out.println("Busted MIN Value = " + (myMinIntValue - 1)); //overflow

        int myMaxIntTest = 2_147_483_647;

        //L25 byte, short, long and width
        byte myMinByteValue = Byte.MIN_VALUE;
        byte myMaxByteValue = Byte.MAX_VALUE;
        System.out.println("Byte min value = " + myMinByteValue);
        System.out.println("Byte max value = " + myMaxByteValue);

        short myMinShortValue = Short.MIN_VALUE;
        short myMaxShortValue = Short.MAX_VALUE;
        System.out.println("Short min value = " + myMinShortValue);
        System.out.println("Short max value = " + myMaxShortValue);

        long myLongValue = 100L;
        long myMinLongValue = Long.MIN_VALUE;
        long myMaxLongValue = Long.MAX_VALUE;
        System.out.println("Long min value = " + myMinLongValue);
        System.out.println("Long max value = " + myMaxLongValue);
        long bigLongLiteralValue = 2_147_483_647_234L;

        //L26 Casting in Java
        short bigShortLiteralValue = 32767;
        int myTotal = myMinIntValue / 2;
        byte myNewByteValue = (byte) (myMinByteValue / 2);

        //L28 float and double Primitive Types

        float myMinFloatValue = Float.MIN_VALUE;
        float myMaxFloatValue = Float.MAX_VALUE;
        System.out.println("Float min value = " + myMinFloatValue);
        System.out.println("Float max value = " + myMaxFloatValue);

        double myMinDoubleValue = Double.MIN_VALUE;
        double myMaxDoubleValue = Double.MAX_VALUE;
        System.out.println("Double min value = " + myMinDoubleValue);
        System.out.println("Double max value = " + myMaxDoubleValue);

        int myIntValue = 5;
        float myFloatValue = 5.25F;
        double myDoubleValue = 5.25D;

        //L29 Floating Point Precision and a Challenge
        System.out.println("myIntValue = " + myIntValue);
        System.out.println("myFloatValue = " + myFloatValue);
        System.out.println("myDoubleValue = " + myDoubleValue);

        double numberOfPounds = 200;
        double convertedToKilograms = numberOfPounds * 0.45359237;
        System.out.println(convertedToKilograms + "kg");


    }
}
