package com.upondev.Section3_FirstSteps.L38_TernaryOperator;

public class App {
    public static void main(String[] args) {

        boolean isCar = true;
        boolean wasCar = isCar ? true : false;
        if (wasCar) {
            System.out.println("wasCar is true");
        }
    }
}
