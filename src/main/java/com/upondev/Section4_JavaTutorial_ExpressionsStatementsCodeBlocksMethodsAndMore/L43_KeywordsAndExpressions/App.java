package com.upondev.Section4_JavaTutorial_ExpressionsStatementsCodeBlocksMethodsAndMore.L43_KeywordsAndExpressions;

public class App {
    public static void main(String[] args) {
        // List of Java keywords
        // https://en.wikipedia.org/wiki/List_of_Java_keywords

        // a mile is equal to 1.609344 kilometers
        double kilometers = (100 * 1.609344);
        //Expressions  - variables, values and operators - kilometers = (100 * 1.609344)
        // Java statement -  double kilometers = (100 * 1.609344); t.y. expressions su duomenu tipu
        // kas yra () irgi yra expressions

        int highScore = 50;

        if (highScore == 50) {
            System.out.println("This is an expression");

        }


        // In the following code that I will type below, write down what parts of the code
        // are expressions.
        int score = 100;
        if (score > 99) {
            System.out.println("You got the high score");
            score = 0;
        }
    }
}
