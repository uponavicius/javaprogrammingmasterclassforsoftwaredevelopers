package com.upondev.Section4_JavaTutorial_ExpressionsStatementsCodeBlocksMethodsAndMore.L44_StatementsWhitespaceAndIndentation_CodeOrganization;

public class App {
    public static void main(String[] args) {
        int myVariable = 50;

        if (myVariable == 50) {
            System.out.println("Printed");
        }
        
        myVariable++;
        myVariable--;
        System.out.println("This is a test");

        System.out.println("This is" +
                " another" +
                " still more.");

        int anotherVariable = 50;
        myVariable--;
        System.out.println("This is another one");
    }
}
